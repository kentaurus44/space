using UnityEngine;

public class GameInit : MonoBehaviour
{
    public void Awake()
    {
		Request.RequestManager.Instance.Init();
		Core.Flow.FlowManager.Instance.Init();
		Core.CustomCamera.CameraManager.Instance.Init();
    }
}